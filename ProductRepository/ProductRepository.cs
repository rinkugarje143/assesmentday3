﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3.ProductRepository
{
    internal class ProductRepository
    {
        //declare class
        Product[] products;
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Acer","Laptop",50000,4.0f),
                new Product("Dell","Laptop",50000,4.5f),
                new Product("Samsung","Mobile",50000,3.9f),
                new Product("Apple", "Mobile",90000,5.0f)
            };
        }

        public Product[] GetAllProducts()
        {
            return products;
        }
        public Product[] GetCatProduct()
        {
            return products;
        }

        

    }
}
