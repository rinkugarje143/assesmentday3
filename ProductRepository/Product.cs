﻿namespace Day3.ProductRepository
{
    internal class Product
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }

        public Product(string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;
            Price = price;
            Rating = rating;
        }
        //method 
        public override string ToString()
        {
            return $"Name :{Name}\t Category :{Category}\t Price : {Price}\t Price : {Rating}";
        }
    }
}